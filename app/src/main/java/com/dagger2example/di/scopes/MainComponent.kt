package com.dagger2example.di.scopes

import com.dagger2example.model.MainInteractor
import com.dagger2example.model.MainInteractorImpl
import com.dagger2example.ui.MainActivity
import com.dagger2example.ui.MainPresenter
import dagger.Binds
import dagger.Module
import dagger.Subcomponent
import ru.mobiledimension.mega.di.utils.ScopeProvider
import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class MainScope

@Subcomponent(modules = [MainModule::class])
@MainScope
interface MainComponent {

    @Subcomponent.Builder interface Builder { fun build(): MainComponent }

    fun inject(activity: MainActivity)
    fun presenter(): MainPresenter
}

@Module abstract class MainModule {
    @Binds abstract fun provideInteractor(int: MainInteractorImpl): MainInteractor
}

object MainScopeProvider : ScopeProvider<MainComponent>() {
    override fun open(): MainComponent {
        return set(component ?: AppScopeProvider.get().mainComponentBuilder().build())
    }
}