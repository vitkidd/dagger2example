package com.dagger2example.di.scopes

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.dagger2example.di.data.NetworkModule
import com.dagger2example.di.data.SettingsModule
import com.dagger2example.utils.ResourcesManager
import com.dagger2example.utils.ResourcesManagerImpl
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Scope


@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AppScope

@AppScope
@Component(modules = [AppModule::class, NetworkModule::class, SettingsModule::class])
interface AppComponent {
    fun mainComponentBuilder(): MainComponent.Builder
}
@Module
class AppModule(private val app: Application) {
    @Provides
    @AppScope
    fun provideApplication(): Application = app

    @Provides
    @AppScope
    fun providePreferences(): SharedPreferences = app.getSharedPreferences("prefs", Context.MODE_PRIVATE)

    @Provides
    @AppScope
    fun provideResourcesManager(): ResourcesManager = ResourcesManagerImpl(app)
}

object AppScopeProvider {

    private lateinit var appComponent: AppComponent

    fun open(app: Application) {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(app))
                .build()
    }

    fun get(): AppComponent = appComponent
}