package com.dagger2example.di.data

import com.dagger2example.di.scopes.AppScope
import com.dagger2example.network.Api
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class NetworkModule {

    @Provides @AppScope fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .readTimeout(READ_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
                .connectTimeout(CONNECTION_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
                .build()
    }

    @Provides @AppScope fun provideGSON(): Gson =GsonBuilder().create()

    @Provides @AppScope fun provideRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }

    @Provides @AppScope fun provideAuthApi(retrofit: Retrofit): Api = retrofit.create(Api::class.java)

    companion object {
        private val BASE_URL = ""
        private val CONNECTION_TIMEOUT = 5 * 1000
        private val READ_TIMEOUT = 7 * 1000
    }
}
