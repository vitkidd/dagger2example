package com.dagger2example.di.data

import android.content.SharedPreferences
import com.dagger2example.di.scopes.AppScope
import com.dagger2example.utils.AppSettings
import com.dagger2example.utils.AppSettingsImpl
import dagger.Module
import dagger.Provides

@Module
class SettingsModule {
    @Provides @AppScope fun provideAppSettings(prefs: SharedPreferences): AppSettings = AppSettingsImpl(prefs)
}
