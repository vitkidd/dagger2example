package ru.mobiledimension.mega.di.utils

abstract class ScopeProvider<Component> {

    protected var component: Component? by ScopeDelegate()

    fun get() = component ?: open()

    fun close() {
        component = null
    }

    abstract fun open(): Component

    protected fun set(newComponent: Component): Component {
        component = newComponent

        return component!!
    }
}
