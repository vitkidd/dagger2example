package com.dagger2example.ui

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.dagger2example.model.MainInteractor
import javax.inject.Inject

@InjectViewState
class MainPresenter @Inject constructor(private val mainInteractor: MainInteractor) : MvpPresenter<MainView>() {

    fun getToast() {
        viewState.showToast(mainInteractor.getSomething())
    }
}
