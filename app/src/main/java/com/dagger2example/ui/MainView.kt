package com.dagger2example.ui

import com.arellomobile.mvp.MvpView

interface MainView : MvpView {
    fun showToast(message: String)
}
