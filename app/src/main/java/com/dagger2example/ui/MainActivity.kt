package com.dagger2example.ui

import android.os.Bundle
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.dagger2example.R
import com.dagger2example.di.scopes.MainScopeProvider
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : MvpAppCompatActivity(), MainView {

    @InjectPresenter lateinit var presenter: MainPresenter

    @ProvidePresenter fun providePresenter(): MainPresenter? = MainScopeProvider.get().presenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        MainScopeProvider.open()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener { presenter.getToast() }
    }

    override fun onStop() {
        super.onStop()
        MainScopeProvider.close()
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}

