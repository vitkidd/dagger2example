package com.dagger2example.model

import com.dagger2example.di.scopes.MainScope
import com.dagger2example.utils.AppSettings
import com.dagger2example.utils.ResourcesManager
import javax.inject.Inject

//interface MainRepository {
//    fun getSomething(): String
//}

@MainScope
class MainRepository @Inject constructor(
        private val appSettings: AppSettings,
        private val res: ResourcesManager
) {
    fun getSomething(): String = appSettings.isFirstLaunch().toString()
}
