package com.dagger2example.model

import com.dagger2example.R
import com.dagger2example.utils.ResourcesManager
import javax.inject.Inject

interface MainInteractor {
    fun getSomething(): String
}

class MainInteractorImpl @Inject constructor(
        private val mainRepository: MainRepository,
        private val resourcesManager: ResourcesManager
) : MainInteractor {
    override fun getSomething(): String {
        return mainRepository.getSomething() + resourcesManager.getString(R.string.app_name)
    }
}