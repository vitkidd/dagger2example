package com.dagger2example

import android.app.Application
import com.dagger2example.di.scopes.AppScopeProvider

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        AppScopeProvider.open(this)
    }
}