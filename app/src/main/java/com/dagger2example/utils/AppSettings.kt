package com.dagger2example.utils

import android.content.SharedPreferences

interface AppSettings {
    fun isFirstLaunch(): Boolean
    fun setFirstLaunch()
}

class AppSettingsImpl(private val preferences: SharedPreferences) : AppSettings {

    override fun isFirstLaunch(): Boolean = preferences.getBoolean(FIRST_LAUNCH, false)
    override fun setFirstLaunch() = preferences.edit().putBoolean(FIRST_LAUNCH, true).apply()

    companion object {
        private val FIRST_LAUNCH = "FIRST_LAUNCH"
    }
}
