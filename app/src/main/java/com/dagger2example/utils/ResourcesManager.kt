package com.dagger2example.utils

import android.content.Context
import android.support.annotation.StringRes

interface ResourcesManager {
    fun getString(@StringRes resId: Int): String
    fun getString(@StringRes resId: Int, vararg format: Any): String
}

class ResourcesManagerImpl(private val context: Context) : ResourcesManager {
    override fun getString(@StringRes resId: Int): String = context.getString(resId)
    override fun getString(@StringRes resId: Int, vararg format: Any): String = context.getString(resId, *format)
}