package com.dagger2example.network

import io.reactivex.Single
import retrofit2.http.POST

interface Api {
    @POST("/something") fun getSomething(): Single<String>
}
