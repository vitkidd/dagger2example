
import com.dagger2example.R
import com.dagger2example.model.MainInteractor
import com.dagger2example.model.MainInteractorImpl
import com.dagger2example.model.MainRepository
import com.dagger2example.utils.ResourcesManagerImpl
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

class AuthInteractorTest {

    private lateinit var interactor: MainInteractor
    private lateinit var authRepo: MainRepository
    private lateinit var resources: ResourcesManagerImpl

    @Before
    fun setUp() {
        authRepo = Mockito.mock(MainRepository::class.java)
        resources = Mockito.mock(ResourcesManagerImpl::class.java)
        interactor = MainInteractorImpl(authRepo, resources)
    }

    @Test
    fun test() {
        `when`(authRepo.getSomething()).thenReturn("test")
        `when`(resources.getString(R.string.app_name)).thenReturn("test")

        val string = interactor.getSomething()

        Assert.assertTrue(string == "testtest")
    }

    @Test
    fun test2() {
        `when`(authRepo.getSomething()).thenReturn("ee")
        `when`(resources.getString(R.string.app_name)).thenReturn("44")

        val string = interactor.getSomething()

        Assert.assertTrue(string == "ee44")
    }

    @Test
    fun test3() {
        `when`(authRepo.getSomething()).thenReturn("dd")
        `when`(resources.getString(R.string.app_name)).thenReturn("45")

        val string = interactor.getSomething()

        Assert.assertTrue(string == "dd45")
    }
}